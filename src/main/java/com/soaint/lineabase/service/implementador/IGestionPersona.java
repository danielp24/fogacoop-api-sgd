package com.soaint.lineabase.service.implementador;

import com.soaint.lineabase.commons.domains.request.PersonaDTORequest;
import com.soaint.lineabase.model.entities.Persona;

import java.util.Collection;
import java.util.Optional;

public interface IGestionPersona {

    Optional<Persona> registerPersona(final PersonaDTORequest persona);

    Optional<Collection<Persona>> findPersonas();

    Optional<Persona> getPersonaById(String id);

}
